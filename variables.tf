variable "aws_region" {
  default = "us-east-2"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "subnet1_cidr_block" {
  default = "10.0.1.0/24"
}

variable "subnet2_cidr_block" {
  default = "10.0.2.0/24"
}

variable "avail_zone1" {
  default = "us-east-2a"
}

variable "avail_zone2" {
  default = "us-east-2b"
}


variable "env_prefix" {
  default = "test"
}